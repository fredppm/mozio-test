package com.example.fredppm.toddsyndrome.app;

import com.example.fredppm.toddsyndrome.core.DiagnosticModel;
import com.example.fredppm.toddsyndrome.core.ToddySyndromeDiagnostic;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by fredppm on 25/01/2017.
 * Repository unit tests
 */

public class RepositoryUnitTest {


    private static final String VAR_NAME = "any name";
    private static final double VAR_PERCENTAGE = 22.2;

    private  DiagnosticMemoryRepository repository;

    public RepositoryUnitTest() {

        List<DiagnosticModel> db = new ArrayList<DiagnosticModel>();

        DiagnosticModel singleModel = new DiagnosticModel();
        singleModel.setPatientName(VAR_NAME);
        singleModel.setPercentage(VAR_PERCENTAGE);

        db.add(singleModel);

        repository = new DiagnosticMemoryRepository(db);

    }


    @Test
    public void List() {
        assertEquals(1, repository.ListAll().size());
        assertEquals(VAR_NAME, repository.ListAll().get(0).getPatientName());
        assertEquals(VAR_PERCENTAGE, repository.ListAll().get(0).getPercentage(), 1);
    }

    @Test
    public void Add() {
        assertEquals(1, repository.ListAll().size());

        String newName = "newName";
        Double newPercentage = 12.3;

        DiagnosticModel model = new DiagnosticModel();
        model.setPatientName(newName);
        model.setPercentage(newPercentage);

        repository.Add(model);


        assertEquals(2, repository.ListAll().size());
        assertEquals(VAR_NAME, repository.ListAll().get(0).getPatientName());
        assertEquals(VAR_PERCENTAGE, repository.ListAll().get(0).getPercentage(), 1);
        assertEquals(newName, repository.ListAll().get(1).getPatientName());
        assertEquals(newPercentage, repository.ListAll().get(1).getPercentage(), 1);
    }
}
