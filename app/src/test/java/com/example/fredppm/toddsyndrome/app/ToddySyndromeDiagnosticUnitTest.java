package com.example.fredppm.toddsyndrome.app;

import com.example.fredppm.toddsyndrome.core.ToddySyndromeDiagnostic;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by fredppm on 25/01/2017.
 * Toddy Syndrome diagnostic unit tests
 */

public class ToddySyndromeDiagnosticUnitTest {


    private static final String VAR_NAME = "any name";
    private ToddySyndromeDiagnostic toddySyndromeDiagnostic;
    private  DiagnosticMemoryRepository repository;

    public ToddySyndromeDiagnosticUnitTest() {
        repository = new DiagnosticMemoryRepository();

        toddySyndromeDiagnostic = new ToddySyndromeDiagnostic(repository);
    }


    @Test
    public void calculate_100() throws Exception {
        assertEquals(0, repository.ListAll().size());

        float result = toddySyndromeDiagnostic.CalculatePercentage(VAR_NAME, true, true, true, true);

        assertEquals(100, result, 1);
        assertEquals(1, repository.ListAll().size());
        assertEquals(VAR_NAME, repository.ListAll().get(0).getPatientName());
        assertEquals(result, repository.ListAll().get(0).getPercentage(), 1);
    }


    @Test
    public void calculate_50() throws Exception {
        assertEquals(0, repository.ListAll().size());

        float result = toddySyndromeDiagnostic.CalculatePercentage(VAR_NAME, true, false, true, false);

        assertEquals(50, result, 1);
        assertEquals(1, repository.ListAll().size());
        assertEquals(VAR_NAME, repository.ListAll().get(0).getPatientName());
        assertEquals(result, repository.ListAll().get(0).getPercentage(), 1);
    }

    @Test
    public void calculate_0() throws Exception {
        assertEquals(0, repository.ListAll().size());

        float result = toddySyndromeDiagnostic.CalculatePercentage(VAR_NAME, false, false, false, false);

        assertEquals(0, result, 1);
        assertEquals(1, repository.ListAll().size());
        assertEquals(VAR_NAME, repository.ListAll().get(0).getPatientName());
        assertEquals(result, repository.ListAll().get(0).getPercentage(), 1);
    }
}