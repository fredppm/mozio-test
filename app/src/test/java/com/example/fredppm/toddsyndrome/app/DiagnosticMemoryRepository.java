package com.example.fredppm.toddsyndrome.app;

import com.example.fredppm.toddsyndrome.core.DiagnosticModel;
import com.example.fredppm.toddsyndrome.core.IDiagnosticRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fredppm on 25/01/2017.
 * Help class to store data in memory.
 */

class DiagnosticMemoryRepository implements IDiagnosticRepository {

    private List<DiagnosticModel> db;

    DiagnosticMemoryRepository() {
        db = new ArrayList();
    }

    DiagnosticMemoryRepository(List<DiagnosticModel> db) {
        this.db = db;
    }

    @Override
    public void Add(DiagnosticModel model) {
        db.add(model);
    }

    @Override
    public List<DiagnosticModel> ListAll() {
        return db;
    }
}
