package com.example.fredppm.toddsyndrome.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.example.fredppm.app.R;
import com.example.fredppm.toddsyndrome.app.data.DiagnosticSQLiteRepository;
import com.example.fredppm.toddsyndrome.core.ToddySyndromeDiagnostic;


public class NewCaseActivity extends AppCompatActivity {

    ToddySyndromeDiagnostic toddySyndromeDiagnostic;

    ViewFlipper viewFlipper;
    TextView textViewDiagnostic;
    EditText editTextPatientName;


    String patientName;
    boolean answerHasMigraines;
    boolean answerYounger15;
    boolean answerIsMan;
    boolean answerUserHallucinogenic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_case);

        DiagnosticSQLiteRepository diagnosticReposity = new DiagnosticSQLiteRepository(this);
        toddySyndromeDiagnostic = new ToddySyndromeDiagnostic(diagnosticReposity);

        textViewDiagnostic = (TextView) findViewById(R.id.textViewDiagnosticPercent);
        viewFlipper = (ViewFlipper) findViewById(R.id.activity_new_case);
        editTextPatientName = (EditText) findViewById(R.id.editTextPatientName);
    }

    public void BackToDashboard(View view) {
        finish();
    }

    public void AddAnother(View view) {
        editTextPatientName.setText("");
        SetStep(R.id.question_layout0);
    }
    public void StartQuestionary(View view) {

        patientName = editTextPatientName.getText().toString();

        SetStep(R.id.question_layout1);
    }


    public void Answer(View view) {
        int viewId = view.getId();

        boolean isYes = false;
        switch (viewId) {
            case R.id.no_button1:
            case R.id.no_button2:
            case R.id.no_button3:
            case R.id.no_button4:
                isYes = false;
                break;
            case R.id.yes_button1:
            case R.id.yes_button2:
            case R.id.yes_button3:
            case R.id.yes_button4:
                isYes = true;
                break;
        }


        switch (viewId) {
            case R.id.no_button1:
            case R.id.yes_button1:
                answerHasMigraines = isYes;
                SetStep(R.id.question_layout2);
                break;
            case R.id.no_button2:
            case R.id.yes_button2:
                answerYounger15 = isYes;
                SetStep(R.id.question_layout3);
                break;
            case R.id.no_button3:
            case R.id.yes_button3:
                answerIsMan = isYes;
                SetStep(R.id.question_layout4);
                break;
            case R.id.no_button4:
            case R.id.yes_button4:
                answerUserHallucinogenic = isYes;

                float parcentage = toddySyndromeDiagnostic.CalculatePercentage(patientName, answerHasMigraines, answerYounger15, answerIsMan, answerUserHallucinogenic);
                textViewDiagnostic.setText(parcentage + "%");
                SetStep(R.id.question_layout5);
                break;
        }

    }

    private void SetStep(int id) {
        viewFlipper.setInAnimation(NewCaseActivity.this, R.anim.view_transition_in_left);
        viewFlipper.setOutAnimation(NewCaseActivity.this, R.anim.view_transition_out_left);
        viewFlipper.setDisplayedChild(viewFlipper.indexOfChild(findViewById(id)));
    }
}
