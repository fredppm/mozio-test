package com.example.fredppm.toddsyndrome.app.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.fredppm.toddsyndrome.core.DiagnosticModel;
import com.example.fredppm.toddsyndrome.core.IDiagnosticRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fredppm on 25/01/2017.
 * Implementation of Diagnostic Repository storing in sql lite (local data base)
 */

public class DiagnosticSQLiteRepository  extends SQLiteOpenHelper implements IDiagnosticRepository {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "toddsydrome";


    private static final String TABLE_DIAGNOSTIC = "diagnostic";

    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_PERCENTAGE = "percentage";

    public DiagnosticSQLiteRepository(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_DIAGNOSTIC + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_PERCENTAGE + " FLOAT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DIAGNOSTIC);

        // Create tables again
        onCreate(db);
    }

    @Override
    public void Add(DiagnosticModel model) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, model.getPatientName());
        values.put(KEY_PERCENTAGE, model.getPercentage());

        db.insert(TABLE_DIAGNOSTIC, null, values);
        db.close();
    }

    @Override
    public List<DiagnosticModel> ListAll() {
        List<DiagnosticModel> list = new ArrayList<DiagnosticModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_DIAGNOSTIC;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                DiagnosticModel model = new DiagnosticModel();
                model.setId(Integer.parseInt(cursor.getString(0)));
                model.setPatientName(cursor.getString(1));
                model.setPercentage(cursor.getFloat(2));
                list.add(model);
            } while (cursor.moveToNext());
        }

        return list;
    }

}
