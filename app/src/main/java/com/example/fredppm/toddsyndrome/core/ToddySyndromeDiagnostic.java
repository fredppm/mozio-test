package com.example.fredppm.toddsyndrome.core;

/**
 * Created by fredppm on 24/01/2017.
 * main class in program, it is used to calculate and store the result.
 */

public class ToddySyndromeDiagnostic {

    private IDiagnosticRepository repository;

    public ToddySyndromeDiagnostic(IDiagnosticRepository repository) {
        this.repository = repository;
    }


    public float CalculatePercentage(String patientName, boolean hasMigraines, boolean younger15, boolean isMan, boolean userHallucinogenic) {

        float points = 0;

        if (hasMigraines)
            points++;

        if (younger15)
            points++;

        if (isMan)
            points++;

        if (userHallucinogenic)
            points++;


        float result =  points / 4 * 100;

        DiagnosticModel model = new DiagnosticModel();
        model.setPercentage(result);
        model.setPatientName(patientName);
        repository.Add(model);

        return result;
    }

}