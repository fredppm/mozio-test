package com.example.fredppm.toddsyndrome.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.example.fredppm.app.R;
import com.example.fredppm.toddsyndrome.app.data.DiagnosticSQLiteRepository;

public class DashboardActivity extends AppCompatActivity {

    DiagnosticSQLiteRepository diagnosticReposity;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);


        listView = (ListView) findViewById(R.id.diagnosticList);
        diagnosticReposity = new DiagnosticSQLiteRepository(this);

        DiagnosticArrayAdapter adapter = new DiagnosticArrayAdapter(this, diagnosticReposity.ListAll());
        listView.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        DiagnosticArrayAdapter adapter = new DiagnosticArrayAdapter(this, diagnosticReposity.ListAll());
        listView.setAdapter(adapter);
    }

    public void addNewCase(View view) {
        Intent intent = new Intent(DashboardActivity.this, NewCaseActivity.class);
        startActivity(intent);
    }
}
