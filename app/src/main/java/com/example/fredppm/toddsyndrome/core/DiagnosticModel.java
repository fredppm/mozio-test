package com.example.fredppm.toddsyndrome.core;

/**
 * Created by fredppm on 25/01/2017.
 * struct data that representated a diagnostic result.
 */

public class DiagnosticModel {
    private Integer id;
    private String patientName;
    private double percentage;

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
