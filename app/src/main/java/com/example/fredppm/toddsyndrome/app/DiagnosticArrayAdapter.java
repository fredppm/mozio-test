package com.example.fredppm.toddsyndrome.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.fredppm.app.R;
import com.example.fredppm.toddsyndrome.core.DiagnosticModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fredppm on 25/01/2017.
 * Simple adapter to be used in interface layout
 */

class DiagnosticArrayAdapter extends ArrayAdapter<DiagnosticModel>   {


    private ArrayList<DiagnosticModel> dataSe;
    private Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView textViewName;
        TextView textViewPercentage;
    }

    DiagnosticArrayAdapter(Context context, List<DiagnosticModel> data) {
        super(context, R.layout.diagnostic_list_item, data);
        this.mContext=context;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DiagnosticModel dataModel = getItem(position);

        ViewHolder viewHolder;

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();

            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.diagnostic_list_item, parent, false);
            viewHolder.textViewName = (TextView) convertView.findViewById(R.id.textViewName);
            viewHolder.textViewPercentage = (TextView) convertView.findViewById(R.id.textViewPercentage);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.down_from_top);
        result.startAnimation(animation);

        viewHolder.textViewName.setText(dataModel.getPatientName());
        viewHolder.textViewPercentage.setText(dataModel.getPercentage() + "%");

        return convertView;
    }
}
