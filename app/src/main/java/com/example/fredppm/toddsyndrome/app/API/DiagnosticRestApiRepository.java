package com.example.fredppm.toddsyndrome.app.API;

import com.example.fredppm.toddsyndrome.core.DiagnosticModel;
import com.example.fredppm.toddsyndrome.core.IDiagnosticRepository;

import retrofit2.Retrofit;

import java.util.List;

/**
 * Created by fredppm on 25/01/2017.
 * This is the implamentation of diagnostic repository in api logic.
 */

public class DiagnosticRestApiRepository implements IDiagnosticRepository {

    private static final String BASE_URL = "http://api.example.com/";
    private DiagnosticAPIProxy proxy;


    public  DiagnosticRestApiRepository() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .build();

        proxy = retrofit.create(DiagnosticAPIProxy.class);
    }


    @Override
    public void Add(DiagnosticModel model) {
        proxy.add(model);
    }

    @Override
    public List<DiagnosticModel> ListAll() {
        return proxy.list();
    }
}
