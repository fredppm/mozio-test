package com.example.fredppm.toddsyndrome.app.API;

import com.example.fredppm.toddsyndrome.core.DiagnosticModel;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by fredppm on 25/01/2017.
 *
 * this is a sample how to call an api. it use retrofit2 to proxy the web call
 */


interface DiagnosticAPIProxy {
    @GET("/diagnostic")
    List<DiagnosticModel> list();

    @POST("/diagnostic")
    void add(@Path("DiagnosticModel") DiagnosticModel model);
}
