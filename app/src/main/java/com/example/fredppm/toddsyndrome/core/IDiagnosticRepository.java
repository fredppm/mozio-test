package com.example.fredppm.toddsyndrome.core;

import java.util.List;

/**
 * Created by fredppm on 25/01/2017.
 * Interface used to encapsulate multiple possibility to store the data.
 */

public interface IDiagnosticRepository {

    void Add(DiagnosticModel model);

    List<DiagnosticModel> ListAll();
}
